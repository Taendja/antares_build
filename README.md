# ANTARES

The VR demonstrator of the ANTARES project uses the Leap Motion Sensor to enable interactions with one's own hands. In addition, various interaction possibilities as well as hand models are available.

The aim of the project is to transfer the Approach Avoidance Task (AAT) into virtual reality in order to develop an accompanying therapy method for the treatment of substance dependence diseases that addresses the impulsive processes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

Information about the use of the demonstrator can be found in the manual - see manual.pdf.

### Prerequisites

In order to use the demonstrator, the Leap Motion SDK must first be installed. This can be found at the following URL:
https://developer.leapmotion.com/get-started/

## Authors

* **Tanja Joan Eiler** - *Initial work* - [Taendja] (https://gitlab.com/Taendja)
* **Benjamin Haßler** - *Supporting work*
* **Vanessa Schmücker** - *Supporting work*

## License

This project is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0) License - see https://creativecommons.org/licenses/by-nc-nd/4.0/ for details